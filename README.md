# Project 2: Stylized Festival Stage Generator #

### Summary ###
This project uses a RunwayML model trained with several hundred images of concert/festival stages. This OpenFrameworks code works to generate a new
stage everytime the spacebar is pressed and save the image whenever 's' is pressed. This way the user is able to save their favorite randomely 
generated images whenever theyd like to. 

### Example ###
![alt text](https://bitbucket.org/bbecco1313/project2/src/master/bin/data/Concert0.jpg "example")
