#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//setup runwayML
	runwayConcert.setup(this, "http://localhost:8000");
	runwayConcert.start();

	//allocate resulting image
	runwayResult.allocate(512, 512, OF_IMAGE_COLOR);

	for (int i = 0; i < 512; i++) {
		//populate the vector with random floats to produce generated stage
		float f = ofRandom(-3.0, 3.0);
		v.push_back(f);
	}
	//counter to save multiple image files 
	counter = 0;
}

//--------------------------------------------------------------
void ofApp::update(){
	//make sure runway is ready
	if (!runwayConcert.isBusy()) {
		ofxRunwayData data;
		//set data to vector of floats
		data.setFloats("z", v);
		data.setFloat("truncation", 0.1);
		//send data to runway to output unique stage
		runwayConcert.send(data);
	}
	//get the resulting image from runway
	runwayConcert.get("image", runwayResult);

}

//--------------------------------------------------------------
void ofApp::draw(){
	//draw instructions (screenshot will leave this part out)
	ofSetColor(0);
	ofDrawRectangle(0, 600, 512, 88);
	ofDrawBitmapStringHighlight("Welcome to the Stylized Festival Stage Generator", 10, 540);
	ofDrawBitmapStringHighlight("Press SPACEBAR to generate a new image, 'S' to save the image", 10, 580);
	ofSetColor(255);

	if (runwayResult.isAllocated()) {
		//draw resulting image
		runwayResult.draw(0, 0);
	}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//generate new image on space bar press
	if (key == ' ') {
		//clear old values
		v.clear();
		for (int i = 0; i < 512; i++) {
			//repopulate vector with random values
			float f = ofRandom(-2.0, 2.0);
			v.push_back(f);
		}
	}
	//save images when s is pressed
	if (key == 's') {
		//grab only part of screen with generated image
		toSave.grabScreen(0, 0, 512, 512);
		//use counter to enable saving many different images
		toSave.save(("Concert" + to_string(counter) + ".jpg"), OF_IMAGE_QUALITY_BEST);;
		counter++; //increment counter
	}
}

void ofApp::runwayInfoEvent(ofJson & info)
{
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}

void ofApp::runwayErrorEvent(string & message)
{
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}
