#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener {

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		
		ofxRunway runwayConcert;
		ofImage runwayResult;
		ofImage toSave;

		vector<float> v;
		int counter;

		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);
		
};
